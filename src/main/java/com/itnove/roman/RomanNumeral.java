package com.itnove.roman;

import java.util.HashMap;
import java.util.Map;

public class RomanNumeral {

    private Map<String, Integer> table;

    public RomanNumeral() {
        table = new HashMap<String, Integer>();
        table.put("I", 1);
        table.put("V", 5);
        table.put("X", 10);
        table.put("L", 50);
        table.put("C", 100);
        table.put("D", 500);
        table.put("M", 1000);
    }

    public int convert(String roman) {
        int counter = 0;
        for(int i=0; i < roman.length();i++) {
            counter += table.get(String.valueOf(roman.charAt(i)));
        }
        return counter;
    }

    private int nextRoman(String roman, int i) {
        return 0;
    }

    public boolean isValid(String roman){
        char[] charArray = roman.toCharArray();
        int count=1;
        for(int i=0; i < charArray.length-1;i++){
            if(String.valueOf(charArray[i]).equals(String.valueOf(charArray[i+1]))){
                count++;
            } else {
                count = 1;
            }
            if(count==4){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        RomanNumeral roman = new RomanNumeral();
        System.out.println(roman.convert("III"));
        if(roman.isValid("IIII")){
            System.out.println("Soc valid");
        } else System.out.println("Soc invalid");
        if(roman.isValid("CCVII")){
            System.out.println("Soc valid");
        } else System.out.println("Soc invalid");
    }

}
